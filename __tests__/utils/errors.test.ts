import {
  GenericError,
  InternalServerError,
  BadRequest,
  Unauthorized,
  Forbidden,
  NotFound
} from '../../src/utils/errors';

describe('Errors', () => {
  describe('Class definition', () => {
    test.each`
      ErrorType
      ${GenericError}
      ${InternalServerError}
      ${BadRequest}
      ${Unauthorized}
      ${Forbidden}
      ${NotFound}
    `('$ErrorType.name should be class with constructor', ({ ErrorType }) => {
      let err: Error | undefined;

      try {
        new ErrorType('error');
      } catch (err) {
        err = err;
      }
      expect(err).toBeUndefined();
      expect.assertions(1);
    });
  });

  describe('Instance of Error', () => {
    test.each`
      ErrorType
      ${GenericError}
      ${InternalServerError}
      ${BadRequest}
      ${Unauthorized}
      ${Forbidden}
      ${NotFound}
    `('$ErrorType.name should be instance of Error', ({ ErrorType }) => {
      expect(new ErrorType('error')).toBeInstanceOf(Error);
    });
  });

  describe('Properties', () => {
    test.each`
      ErrorType              | message                    | httpStatusCode
      ${GenericError}        | ${'generic-error'}         | ${500}
      ${InternalServerError} | ${'internal-server-error'} | ${500}
      ${BadRequest}          | ${'bad-request'}           | ${400}
      ${Unauthorized}        | ${'unauthorized'}          | ${401}
      ${Forbidden}           | ${'forbidden'}             | ${403}
      ${NotFound}            | ${'not-found'}             | ${404}
    `(
      '$ErrorType.name should have required member properties',
      ({ ErrorType, message, httpStatusCode }) => {
        const err = new ErrorType(message);
        expect(err.name).toBe(ErrorType.name);
        expect(err.message).toBe(message);
        expect(err.stack).toBeDefined();
        expect(err.httpStatusCode).toBe(httpStatusCode);
      }
    );
  });
});
