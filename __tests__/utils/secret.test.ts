const mockConsoleLog = jest.spyOn(console, 'log').mockImplementation(msg => msg);
const mockConsoleWarn = jest.spyOn(console, 'warn').mockImplementation(msg => msg);

import { Secret } from '../../src/utils/secret';

beforeEach(jest.resetAllMocks);

describe('Secret', () => {
  describe('Class definition', () => {
    test('should be constructor', () => {
      let err: Error | undefined;

      try {
        new Secret('TEST', 'WORKS', true);
      } catch (err) {
        err = err;
      }
      expect(err).toBeUndefined();
      expect.assertions(1);
    });

    test('should expose secret restart function', () => {
      const secret = new Secret('TEST', 'WORKS', true);
      expect(secret.resetValue).toBeDefined();
      expect(secret.resetValue).toBeInstanceOf(Function);
    });
  });

  describe('Instance creation', () => {
    test('should create new object of type Secret', () => {
      const secret = new Secret('NODE_ENV', undefined, true);
      expect(secret).toBeInstanceOf(Secret);
    });

    test('should create secret if value is defined in `process.env`', () => {
      const secret = new Secret('NODE_ENV', undefined, true);
      expect(secret.value).toBeDefined();
      expect(secret.value).toBe('test');
    });

    test('should create secret if value is undefined in `process.env`, but default is provided', () => {
      const secret = new Secret('NOT_EXISTING', 'SOME_DEFAULT', true);
      expect(secret.value).toBeDefined();
      expect(secret.value).toBe('SOME_DEFAULT');
    });

    test('should throw error if `key` is not provided', () => {
      try {
        new Secret('', 'SOME_VALID_VALUE', true);
      } catch (err) {
        expect(err).toBeInstanceOf(Error);
        expect(err.message).toBe(`Creating Secret '' failed. Key must be valid, non-empty string`);
      }
      expect.assertions(2);
    });

    test('should throw error if value is not defined in `process.env` and no default is passed', () => {
      try {
        new Secret('NON_EXISTING', undefined, true);
      } catch (err) {
        expect(err).toBeInstanceOf(Error);
        expect(err.message).toBe(
          `Creating Secret 'NON_EXISTING' failed. 'process.env.NON_EXISTING' does not exist and default value is not provided`
        );
      }
      expect.assertions(2);
    });
  });

  describe('Console output', () => {
    test('should `console.log` success message for valid secret if in verbose mode', () => {
      new Secret('NODE_ENV', undefined);
      expect(mockConsoleLog).toBeCalledTimes(1);
      expect(mockConsoleLog).toBeCalledWith(`Creating Secret 'NODE_ENV' with value 'test'.`);
    });

    test('should not `console.log` success message for valid secret if not in verbose mode', () => {
      new Secret('NODE_ENV', undefined, true);
      expect(mockConsoleLog).toBeCalledTimes(0);
    });

    test('should `console.warn` for valid secret with default value if in verbose mode', () => {
      new Secret('EXAMPLE', 'default-value', undefined);
      expect(mockConsoleWarn).toBeCalledTimes(1);
      expect(mockConsoleWarn).toBeCalledWith(
        `Creating Secret 'EXAMPLE' using default value 'default-value'.`
      );
    });

    test('should not `console.warn` for valid secret with default value if not in verbose mode', () => {
      new Secret('EXAMPLE', 'default-value', true);
      expect(mockConsoleWarn).toBeCalledTimes(0);
    });
  });

  describe('Getter', () => {
    test('should get secret value and return valid string', () => {
      const secret = new Secret('NODE_ENV', undefined, true);
      expect(secret.value).toBe('test');
    });

    test('should get initial secret value even after being changed', () => {
      const secret = new Secret('EXAMPLE', 'INITIAL_VALUE', true);
      secret.value = 'CHANGED_VALUE';
      expect(secret.resetValue()).toBe('INITIAL_VALUE');
    });
  });

  describe('Setter', () => {
    test('should set secret value and return valid string if value was provided', () => {
      const secret = new Secret('NODE_ENV', undefined, true);
      secret.value = 'some-value';
      expect(secret.value).toBe('some-value');
    });
  });

  describe('Reset to initial value', () => {
    test('should restore the initial secret value and return it', () => {
      const secret = new Secret('NODE_ENV', undefined, true);
      secret.value = 'new-value';
      secret.resetValue();
      expect(secret.value).toBe('test');
    });
  });
});
