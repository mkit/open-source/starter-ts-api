import { LEVELS, createLogger } from '../../src/utils/logger';

describe('Logger', () => {
  describe('Log levels', () => {
    test('should match a pre-defined key-value pair', () => {
      expect(LEVELS).toStrictEqual({
        ALL: 'ALL',
        TRACE: 'TRACE',
        DEBUG: 'DEBUG',
        INFO: 'INFO',
        WARN: 'WARN',
        ERROR: 'ERROR',
        FATAL: 'FATAL',
        OFF: 'OFF'
      });
    });
  });

  describe('Logging visibility depending on the log level', () => {
    test(`should log any message by default`, () => {
      const logger = createLogger();
      expect(logger.isTraceEnabled()).toBeTruthy();
      expect(logger.isDebugEnabled()).toBeTruthy();
      expect(logger.isInfoEnabled()).toBeTruthy();
      expect(logger.isWarnEnabled()).toBeTruthy();
      expect(logger.isErrorEnabled()).toBeTruthy();
      expect(logger.isFatalEnabled()).toBeTruthy();
    });

    test('should log any message if level is incorrect', () => {
      const logger = createLogger('' as LEVELS);
      expect(logger.isTraceEnabled()).toBeTruthy();
      expect(logger.isDebugEnabled()).toBeTruthy();
      expect(logger.isInfoEnabled()).toBeTruthy();
      expect(logger.isWarnEnabled()).toBeTruthy();
      expect(logger.isErrorEnabled()).toBeTruthy();
      expect(logger.isFatalEnabled()).toBeTruthy();
    });

    test(`should log 'ALL' types of messages same as default behavior`, () => {
      const logger = createLogger(LEVELS.ALL);
      expect(logger.isTraceEnabled()).toBeTruthy();
      expect(logger.isDebugEnabled()).toBeTruthy();
      expect(logger.isInfoEnabled()).toBeTruthy();
      expect(logger.isWarnEnabled()).toBeTruthy();
      expect(logger.isErrorEnabled()).toBeTruthy();
      expect(logger.isFatalEnabled()).toBeTruthy();
    });

    test('should log >= `TRACE` type of messages', () => {
      const logger = createLogger(LEVELS.TRACE);
      expect(logger.isTraceEnabled()).toBeTruthy();
      expect(logger.isDebugEnabled()).toBeTruthy();
      expect(logger.isInfoEnabled()).toBeTruthy();
      expect(logger.isWarnEnabled()).toBeTruthy();
      expect(logger.isErrorEnabled()).toBeTruthy();
      expect(logger.isFatalEnabled()).toBeTruthy();
    });

    test('should log >= `DEBUG` type of messages', () => {
      const logger = createLogger(LEVELS.DEBUG);
      expect(logger.isTraceEnabled()).toBeFalsy();
      expect(logger.isDebugEnabled()).toBeTruthy();
      expect(logger.isInfoEnabled()).toBeTruthy();
      expect(logger.isWarnEnabled()).toBeTruthy();
      expect(logger.isErrorEnabled()).toBeTruthy();
      expect(logger.isFatalEnabled()).toBeTruthy();
    });

    test('should log >= `INFO` type of messages', () => {
      const logger = createLogger(LEVELS.INFO);
      expect(logger.isTraceEnabled()).toBeFalsy();
      expect(logger.isDebugEnabled()).toBeFalsy();
      expect(logger.isInfoEnabled()).toBeTruthy();
      expect(logger.isWarnEnabled()).toBeTruthy();
      expect(logger.isErrorEnabled()).toBeTruthy();
      expect(logger.isFatalEnabled()).toBeTruthy();
    });

    test('should log >= `WARN` type of messages', () => {
      const logger = createLogger(LEVELS.WARN);
      expect(logger.isTraceEnabled()).toBeFalsy();
      expect(logger.isDebugEnabled()).toBeFalsy();
      expect(logger.isInfoEnabled()).toBeFalsy();
      expect(logger.isWarnEnabled()).toBeTruthy();
      expect(logger.isErrorEnabled()).toBeTruthy();
      expect(logger.isFatalEnabled()).toBeTruthy();
    });

    test('should log >= `ERROR` type of messages', () => {
      const logger = createLogger(LEVELS.ERROR);
      expect(logger.isTraceEnabled()).toBeFalsy();
      expect(logger.isDebugEnabled()).toBeFalsy();
      expect(logger.isInfoEnabled()).toBeFalsy();
      expect(logger.isWarnEnabled()).toBeFalsy();
      expect(logger.isErrorEnabled()).toBeTruthy();
      expect(logger.isFatalEnabled()).toBeTruthy();
    });

    test('should only `FATAL` type of messages', () => {
      const logger = createLogger(LEVELS.FATAL);
      expect(logger.isTraceEnabled()).toBeFalsy();
      expect(logger.isDebugEnabled()).toBeFalsy();
      expect(logger.isInfoEnabled()).toBeFalsy();
      expect(logger.isWarnEnabled()).toBeFalsy();
      expect(logger.isErrorEnabled()).toBeFalsy();
      expect(logger.isFatalEnabled()).toBeTruthy();
    });

    test('should not log any messages at all by setting level to `OFF`', () => {
      const logger = createLogger(LEVELS.OFF);
      expect(logger.isTraceEnabled()).toBeFalsy();
      expect(logger.isDebugEnabled()).toBeFalsy();
      expect(logger.isInfoEnabled()).toBeFalsy();
      expect(logger.isWarnEnabled()).toBeFalsy();
      expect(logger.isErrorEnabled()).toBeFalsy();
      expect(logger.isFatalEnabled()).toBeFalsy();
    });
  });
});
