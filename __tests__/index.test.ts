import { Greeter } from '../src/index';

describe('Greeter', () => {
  test('should return result of type string', () => {
    expect(typeof Greeter('something')).toBe('string');
  });

  test('should return message in the format of `Hello ${name-passed}`', () => {
    expect(Greeter('John')).toBe('Hello John');
  });
});
