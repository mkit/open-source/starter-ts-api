import { Env } from '../src/env';
import { Secret } from '../src/utils/secret';

jest.mock('../src/utils/secret');
const mockedSecret = Secret as jest.Mock;
mockedSecret.mockImplementation((which, defaultValue?) => ({
  value: process.env[which] || defaultValue || undefined // eslint-disable-line
}));

describe('Env', () => {
  const env = Env.getInstance();

  test('should be singleton', () => {
    const envDouble = Env.getInstance();
    expect(env).toBe(envDouble);
  });

  test('should create N secrets', () => {
    expect(Secret).toBeCalledTimes(2);
  });

  test.each`
    attempt | varName       | defaultValue     | expectedValue
    ${0}    | ${'NODE_ENV'} | ${'development'} | ${'test'}
    ${1}    | ${'PORT'}     | ${'0'}           | ${'0'}
  `(
    'should create $varName secret with default value equal to $defaultValue',
    ({ attempt, varName, defaultValue, expectedValue }) => {
      expect(mockedSecret.mock.calls[attempt][0]).toBe(varName);
      expect(mockedSecret.mock.calls[attempt][1]).toBe(defaultValue);
      expect(env[varName].value).toBe(expectedValue);
    }
  );
});
