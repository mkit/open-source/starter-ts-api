export class Secret {
  private readonly _key: string;
  private readonly _defaultValue: string | undefined;
  private readonly _silent: boolean;
  private readonly _initialValue: string;
  private _value: string;
  public constructor(
    key: string,
    defaultValue: string | undefined = undefined,
    silent: boolean = false
  ) {
    this._key = key;
    this._defaultValue = defaultValue;
    this._silent = silent;

    let valueCandidate: string | undefined = process.env[key]; // eslint-disable-line

    if (!key) {
      const keyError = this.createErrorMessageForKey();
      throw new Error(keyError);
    }

    if (valueCandidate) {
      this._initialValue = this._value = valueCandidate;
      this.printMessageForSuccessfulCreation();
    } else if (defaultValue) {
      this._initialValue = this._value = defaultValue;
      this.printWarningForDefaultUsage();
    } else {
      const valueError = this.createErrorMessageForValue();
      throw new Error(valueError);
    }
  }
  private printMessageForSuccessfulCreation(): void {
    if (!this._silent) {
      console.log(`Creating Secret '${this._key}' with value '${this._value}'.`);
    }
  }
  private printWarningForDefaultUsage(): void {
    if (!this._silent) {
      console.warn(`Creating Secret '${this._key}' using default value '${this._defaultValue}'.`);
    }
  }
  private createErrorMessageForKey(): string {
    return `Creating Secret '${this._key}' failed. Key must be valid, non-empty string`;
  }
  private createErrorMessageForValue(): string {
    return `Creating Secret '${this._key}' failed. 'process.env.${
      this._key
    }' does not exist and default value is not provided`;
  }
  public get value(): string {
    return this._value;
  }
  public set value(newValue: string) {
    this._value = newValue;
  }
  public resetValue(): string {
    this._value = this._initialValue;
    return this._value;
  }
}
