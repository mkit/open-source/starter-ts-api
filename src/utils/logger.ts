import { getLogger, Logger as _Logger } from 'log4js';

export enum LEVELS {
  ALL = 'ALL',
  TRACE = 'TRACE',
  DEBUG = 'DEBUG',
  INFO = 'INFO',
  WARN = 'WARN',
  ERROR = 'ERROR',
  FATAL = 'FATAL',
  OFF = 'OFF'
}

export const createLogger = (
  level: LEVELS = LEVELS.ALL,
  category?: string | undefined
): _Logger => {
  const logger = getLogger(category);
  logger.level = level;

  return logger;
};
