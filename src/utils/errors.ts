/**
 * Generic or unknown error type
 */
export class GenericError extends Error {
  public httpStatusCode: number;
  public constructor(message: string, httpStatusCode?: string | number | undefined) {
    super(message);
    this.name = this.constructor.name;
    this.httpStatusCode = Number(httpStatusCode) || 500;
  }
}

/**
 * 5XX
 */
export class InternalServerError extends GenericError {}

/**
 * 4XX
 */
export class BadRequest extends GenericError {
  public httpStatusCode: number = 400;
}

export class Unauthorized extends GenericError {
  public httpStatusCode: number = 401;
}

export class Forbidden extends GenericError {
  public httpStatusCode: number = 403;
}

export class NotFound extends GenericError {
  public httpStatusCode: number = 404;
}
