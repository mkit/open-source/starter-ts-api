import * as dotenv from 'dotenv-flow';
import { Secret } from './utils/secret';

export class Env {
  private static instance: Env;
  public NODE_ENV: Secret;
  public PORT: Secret;

  [index: string]: Secret;
  private constructor() {
    dotenv.config();
    this.NODE_ENV = new Secret('NODE_ENV', 'development');
    this.PORT = new Secret('PORT', '0');
  }
  public static getInstance(): Env {
    if (!Env.instance) {
      Env.instance = new Env();
    }
    return Env.instance;
  }
}
