/**
 * Global Jest configuration.
 *
 * See available options and defaults at https://jestjs.io/docs/en/configuration
 */

module.exports = {
  /**
   * Use `ts-jest` for all TypeScript file.
   */
  transform: {
    '^.+\\.tsx?$': 'ts-jest'
  },

  /**
   * For node-like environments use `node` instead of the default `jsdom`.
   *
   * See https://jestjs.io/docs/en/configuration.html#testenvironment-string
   * and https://medium.com/@kevinsimper/how-to-disable-jsdom-in-jest-make-jest-run-twice-as-fast-a01193f23405
   */
  testEnvironment: 'node',

  /**
   * Collect coverage report from all src files
   */
  collectCoverageFrom: ['src/**/*.{ts,js}']
};
