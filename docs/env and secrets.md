TODO: 

* why?
* how does src/env and src/utils/secret work together to build reusable, singleton env configuration
* how to add vars
* how .env and similar files get loaded (dotenv-flow)
* what's the usage = how to use in the app?

- See https://www.npmjs.com/package/dotenv-flow#variables-overwritingpriority for variables priority and overwriting
- See https://www.npmjs.com/package/dotenv-flow#files-under-version-control for clarity on which files should be kept in Git